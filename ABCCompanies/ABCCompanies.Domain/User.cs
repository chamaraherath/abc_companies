﻿namespace ABCCompanies.Domain
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public virtual SecurityProfile SecurityProfile { get; set; }
        public int SecurityProfileId { get; set; }
    }
}