﻿using System.Collections.Generic;

namespace ABCCompanies.Domain
{
    public class SecurityProfile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool CanCreate { get; set; }
        public bool CanRead { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}