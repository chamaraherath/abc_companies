﻿using System.Collections.Generic;

namespace ABCCompanies.Domain
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Logo { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}