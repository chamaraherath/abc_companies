﻿using System.Collections.Generic;

namespace ABCCompanies.Domain
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
        public virtual Company Company { get; set; }
        public int CompanyId { get; set; }
    }
}