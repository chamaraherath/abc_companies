﻿using System.Data.Entity.Migrations;
using ABCCompanies.Domain;

namespace ABCCompanies.Data.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CompanyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CompanyDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            SecurityProfile defaultSecutiryProfile = new SecurityProfile
            {
                Id = 1,
                Name = "Administrator",
                CanCreate = true,
                CanRead = true,
                CanUpdate = true,
                CanDelete = true,                
            };

            User defaultUser = new User
            {
                Id = 1,
                FirstName = "CHAMARA",
                LastName = "HERATH",
                Email = "chamaraherathvch@gmail.com",
                SecurityProfile = defaultSecutiryProfile,
                SecurityProfileId = defaultSecutiryProfile.Id,
                Password = "123"
            };            

            context.SecurityProfiles.AddOrUpdate(s => s.Id, defaultSecutiryProfile);
            context.Users.AddOrUpdate(u => u.Id, defaultUser);
        }
    }
}
