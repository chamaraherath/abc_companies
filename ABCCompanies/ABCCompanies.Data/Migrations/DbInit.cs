﻿using System.Data.Entity;
using System.Threading;

namespace ABCCompanies.Data.Migrations
{
    public static class DbInit
    {
        private static int _initialized;
        public static void Initialize()
        {
            var initialized = Interlocked.Exchange(ref _initialized, 1);
            if (initialized != 0) {
                return;
            }

            Database.SetInitializer(new CompanyMigrateDatabaseToLatestVersion());
        }
    }
}