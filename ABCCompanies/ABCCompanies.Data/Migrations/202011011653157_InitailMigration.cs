﻿namespace ABCCompanies.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitailMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Email = c.String(),
                        Website = c.String(),
                        Logo = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        CompanyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Company", t => t.CompanyId, cascadeDelete: true)
                .Index(t => t.CompanyId);
            
            CreateTable(
                "dbo.Tag",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SecurityProfile",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        CanCreate = c.Boolean(nullable: false),
                        CanRead = c.Boolean(nullable: false),
                        CanUpdate = c.Boolean(nullable: false),
                        CanDelete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        SecurityProfileId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SecurityProfile", t => t.SecurityProfileId, cascadeDelete: true)
                .Index(t => t.SecurityProfileId);
            
            CreateTable(
                "dbo.TagEmployee",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false),
                        TagId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.EmployeeId, t.TagId })
                .ForeignKey("dbo.Employee", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.Tag", t => t.TagId, cascadeDelete: true)
                .Index(t => t.EmployeeId)
                .Index(t => t.TagId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.User", "SecurityProfileId", "dbo.SecurityProfile");
            DropForeignKey("dbo.Employee", "CompanyId", "dbo.Company");
            DropForeignKey("dbo.TagEmployee", "TagId", "dbo.Tag");
            DropForeignKey("dbo.TagEmployee", "EmployeeId", "dbo.Employee");
            DropIndex("dbo.TagEmployee", new[] { "TagId" });
            DropIndex("dbo.TagEmployee", new[] { "EmployeeId" });
            DropIndex("dbo.User", new[] { "SecurityProfileId" });
            DropIndex("dbo.Employee", new[] { "CompanyId" });
            DropTable("dbo.TagEmployee");
            DropTable("dbo.User");
            DropTable("dbo.SecurityProfile");
            DropTable("dbo.Tag");
            DropTable("dbo.Employee");
            DropTable("dbo.Company");
        }
    }
}
