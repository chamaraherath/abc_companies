﻿using System.Data.Entity.ModelConfiguration;
using ABCCompanies.Domain;

namespace ABCCompanies.Data.Configurations
{
    public class EmployeeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration()
        {
            ToTable("Employee");

            HasRequired(r => r.Company)
            .WithMany(r => r.Employees)
            .HasForeignKey(t => t.CompanyId);

            HasMany(r => r.Tags)
            .WithMany(r => r.Employees)
            .Map(mc =>
            {
                mc.ToTable("TagEmployee");
                mc.MapLeftKey("TagId");
                mc.MapRightKey("EmployeeId");
            });
        }
    }
}