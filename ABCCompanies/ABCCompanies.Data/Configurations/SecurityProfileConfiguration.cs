﻿using System.Data.Entity.ModelConfiguration;
using ABCCompanies.Domain;

namespace ABCCompanies.Data.Configurations
{
    public class SecurityProfileConfiguration : EntityTypeConfiguration<SecurityProfile>
    {
        public SecurityProfileConfiguration()
        {
            ToTable("SecurityProfile");
            HasKey(s => s.Id);

            HasMany(r => r.Users)
           .WithRequired(c => c.SecurityProfile)
           .HasForeignKey(t => t.SecurityProfileId);
        }
    }
}