﻿using System.Data.Entity.ModelConfiguration;
using ABCCompanies.Domain;

namespace ABCCompanies.Data.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("User");
            HasKey(s => s.Id);

            HasRequired(r => r.SecurityProfile)
            .WithMany()
            .HasForeignKey(t => t.SecurityProfileId);
        }
    }
}