﻿using System.Data.Entity.ModelConfiguration;
using ABCCompanies.Domain;

namespace ABCCompanies.Data.Configurations
{
    public class TagConfiguration :  EntityTypeConfiguration<Tag>
    {
        public TagConfiguration()
        {
            ToTable("Tag");
            HasKey(s => s.Id);

            HasMany(r => r.Employees)
            .WithMany(r => r.Tags)
            .Map(mc =>
            {
                mc.ToTable("TagEmployee");
                mc.MapLeftKey("TagId");
                mc.MapRightKey("EmployeeId");
            });
        }
    }
}