﻿using System.Data.Entity.ModelConfiguration;
using ABCCompanies.Domain;

namespace ABCCompanies.Data.Configurations
{
    public class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            ToTable("Company");
            HasKey(s => s.Id);

            HasMany(r => r.Employees)
            .WithRequired(c => c.Company)
            .HasForeignKey(t => t.CompanyId);
        }
    }
}