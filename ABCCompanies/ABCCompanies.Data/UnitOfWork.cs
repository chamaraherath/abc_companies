﻿using System;
using ABCCompanies.Data;
using ABCCompanies.Data.Contracts;
using ABCCompanies.Domain;

namespace ABCCompanies.Models
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        protected IRepositoryProvider RepositoryProvider { get; set; }

        private CompanyDbContext DbContext;

        public UnitOfWork(IRepositoryProvider repositoryProvider)
        {
            CreateDbContext();
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }

        public IEntityRepository<SecurityProfile> SecurityProfiles => GetStanderedRepo<SecurityProfile>();

        public IUserRepository Users => GetRepo<IUserRepository>();

        public IEntityRepository<Company> Companies => GetStanderedRepo<Company>();

        public IEntityRepository<Employee> Employees => GetStanderedRepo<Employee>();

        public void Dispose()
        {
            DbContext.Dispose();
        }

        public int Save()
        {
            return DbContext.SaveChanges();
        }

        private IEntityRepository<T> GetStanderedRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }

        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }

        protected void CreateDbContext() {
            DbContext = new CompanyDbContext();
            DbContext.Configuration.ProxyCreationEnabled = true;
            DbContext.Configuration.LazyLoadingEnabled = true;
            DbContext.Configuration.ValidateOnSaveEnabled = true;
        }
    }
}