﻿using System.Collections.Generic;
using System.Linq;

namespace ABCCompanies.Data.Contracts
{
    public interface IEntityRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);
        void AddRange(IList<T> entity);
    }
}
