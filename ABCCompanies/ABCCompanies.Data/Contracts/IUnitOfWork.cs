﻿using ABCCompanies.Domain;

namespace ABCCompanies.Data.Contracts
{
    public interface IUnitOfWork
    {
        IEntityRepository<SecurityProfile> SecurityProfiles { get; }
        IUserRepository Users { get; }
        IEntityRepository<Company> Companies { get; }
        IEntityRepository<Employee> Employees { get; }
        int Save();
    }
}
