﻿using ABCCompanies.Domain;

namespace ABCCompanies.Data.Contracts
{
    public interface IUserRepository : IEntityRepository<User>
    {
        User GetUserByEmail(string email);
    }
}
