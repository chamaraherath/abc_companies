﻿using System;
using System.Data.Entity;

namespace ABCCompanies.Data.Contracts
{
    public interface IRepositoryProvider
    {
        DbContext DbContext { get; set; }

        IEntityRepository<T> GetRepositoryForEntityType<T>() where T : class;

        T GetRepository<T>(Func<DbContext, object> factory = null) where T : class;

        void SetRepository<T>(T Repository);
    }
}
