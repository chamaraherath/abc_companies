﻿using System.Data.Entity;
using ABCCompanies.Data.Configurations;
using ABCCompanies.Domain;

namespace ABCCompanies.Data
{
    public class CompanyDbContext : DbContext
    {
        public CompanyDbContext() : base(System.Configuration.ConfigurationManager.ConnectionStrings["CompanyContext"].ConnectionString ?? "name=CompanyDbContext")
        {
        }

        public virtual DbSet<SecurityProfile> SecurityProfiles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CompanyConfiguration());
            modelBuilder.Configurations.Add(new EmployeeConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new SecurityProfileConfiguration());
            modelBuilder.Configurations.Add(new TagConfiguration());
        }
    }
}