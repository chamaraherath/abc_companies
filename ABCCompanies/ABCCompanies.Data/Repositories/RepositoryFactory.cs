﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using ABCCompanies.Data.Contracts;

namespace ABCCompanies.Data.Repositories
{
    public class RepositoryFactories
    {

        private IDictionary<Type, Func<DbContext, object>> GetFactories()
        {
            return
             new Dictionary<Type, Func<DbContext, object>>
             {
                 {typeof(IUserRepository), dbContext => new UserRepository(dbContext)}
             };
        }

        public RepositoryFactories()
        {
            _repositoryFactories = GetFactories();
        }

        public Func<DbContext, object> GetRepositoryFactory<T>()
        {

            Func<DbContext, object> factory;
            _repositoryFactories.TryGetValue(typeof(T), out factory);
            return factory;
        }

        public Func<DbContext, object> GetRepositoryFactoryForEntityType<T>() where T : class
        {
            return GetRepositoryFactory<T>() ?? DefaultEntityRepositoryFactory<T>();
        }

        protected virtual Func<DbContext, object> DefaultEntityRepositoryFactory<T>() where T : class
        {
            return dbContext => new EntityRepository<T>(dbContext);
        }

        private readonly IDictionary<Type, Func<DbContext, object>> _repositoryFactories;

    }
}
