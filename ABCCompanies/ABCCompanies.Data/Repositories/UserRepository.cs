﻿using ABCCompanies.Domain;
using System.Data.Entity;
using System.Linq;
using ABCCompanies.Data.Contracts;

namespace ABCCompanies.Data.Repositories
{
    public class UserRepository : EntityRepository<User>, IUserRepository
    {
        public UserRepository(DbContext dbContext) : base(dbContext)
        {

        }
        public User GetUserByEmail(string email)
        {
            return DbSet.OfType<User>().First(u => u.Email.Equals(email));
        }
    }
}