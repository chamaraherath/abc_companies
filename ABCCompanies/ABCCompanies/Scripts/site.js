﻿$(document).ready(function () {
    $('.drewer-overlay').click(function () {
        $(this).siblings('.drawer-component').hide();
        $(this).hide();
    });
    $('.drawer-close-button').click(function () {
        $(this).parents('.drawer-component').hide();
        $(this).parents('.drawer-component').siblings('.drewer-overlay').hide();
    });
    $('.show-employees-button').click(function () {
        showEmployees($(this).parents('.company-row').attr('data-company-id'));       
    });
    $('#add-new-company-button').click(function () {
        showAddNewCompanyDrawer();
    });
    $('#add-new-user-button').click(function () {
        showAddNewUserDrawer();
    });

    $('#add-security-profile-button').click(function () {
        showAddNewSecurityProfileDrawer();
    });

    $('.edit-company-button').click(function () {
        showEditCompanyDrawer($(this).parents('.company-row').attr('data-company-id'));
    });

    $('.edit-employees-button').click(function () {
        showEditEmployeeDrawer($(this).parents('.employee-row').attr('data-employee-id'));
    });

    $('.edit-security-profile-button').click(function () {
        showEditSecurityProfileDrawer($(this).parents('.security-profile-row').attr('data-security-profile-id'));
    });

    $('.edit-user-button').click(function () {
        showEditUserDrawer($(this).parents('.user-row').attr('data-user-id'));
    });
    $('#user-profile-edit').click(function () {
        showEditUserDrawer($(this).attr('data-user-id'));
    });

    initDrawerEvents();
});

function showEmployeeDrawer() {
    $('#employee-drawer').find('.drawer-component').show();
    $('#employee-drawer').find('.drewer-overlay').show();
}

function showAddNewCompanyDrawer() {
    $('#add-company-drawer').find('.drawer-component').show();
    $('#add-company-drawer').find('.drewer-overlay').show();
}

function showAddNewEmployeeDrawer() {
    $('#add-employee-drawer').find('.drawer-component').show();
    $('#add-employee-drawer').find('.drewer-overlay').show();
    var companyId = $('#employee-list-card').attr('data-company-id');
    getAddEmployeeDrawerContent(companyId)
}

function showAddNewUserDrawer() {
    $('#add-user-drawer').find('.drawer-component').show();
    $('#add-user-drawer').find('.drewer-overlay').show();
    getAddUserDrawerContent();
}

function showAddNewSecurityProfileDrawer() {
    $('#add-security-profile-drawer').find('.drawer-component').show();
    $('#add-security-profile-drawer').find('.drewer-overlay').show();
}

function showEditCompanyDrawer(companyId) {
    $('#edit-company-drawer').find('.drawer-component').show();
    $('#edit-company-drawer').find('.drewer-overlay').show();
    getEditCompanyDrawerContent(companyId)
}

function showEditEmployeeDrawer(employeeId) {
    $('#edit-employee-drawer').find('.drawer-component').show();
    $('#edit-employee-drawer').find('.drewer-overlay').show();
    getEditEmployeeDrawerContent(employeeId)
}

function showEditSecurityProfileDrawer(profileId) {
    $('#edit-security-profile-drawer').find('.drawer-component').show();
    $('#edit-security-profile-drawer').find('.drewer-overlay').show();
    getEditSecurityProfileDrawerContent(profileId)
}

function showEditUserDrawer(userId) {
    $('#edit-user-drawer').find('.drawer-component').show();
    $('#edit-user-drawer').find('.drewer-overlay').show();
    getEditUserDrawerContent(userId)
}

function initDrawerEvents() {
    $('#add-new-member-button').click(function () {
        showAddNewEmployeeDrawer();
    });
}