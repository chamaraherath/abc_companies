﻿using System.Collections.Generic;
using ABCCompanies.Domain;

namespace ABCCompanies.Models.ViewModel
{
    public class EmployeeViewModel
    {
        public EmployeeViewModel(Employee employee)
        {
            Id = employee.Id;
            FirstName = employee.FirstName;
            LastName = employee.LastName;
            Email = employee.Email;
            Phone = employee.Phone;
            Company = new CompanyViewModel(employee.Company);
        }

        public EmployeeViewModel()
        {

        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName  { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public CompanyViewModel Company { get; set; }
        public List<CompanyViewModel> AvailableCompanies { get; set; }
        public List<string> Tags { get; set; }
    }
}