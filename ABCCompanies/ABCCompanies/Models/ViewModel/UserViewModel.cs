﻿using ABCCompanies.Domain;
using System.Collections.Generic;

namespace ABCCompanies.Models.ViewModel
{
    public class UserViewModel
    {
        public UserViewModel()
        {
        }

        public UserViewModel(User user)
        {
            Id = user?.Id;
            FirstName = user?.FirstName;
            LastName = user?.LastName;
            Email = user?.Email;
            Password = user?.Password;
            SecurityProfile = user != null? new SecurityProfileViewModel(user.SecurityProfile) : null;
        }

        public int? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string OldPassword { get; set; }
        public string ValidatePassword { get; set; }
        public SecurityProfileViewModel SecurityProfile { get; set; }
        public List<SecurityProfileViewModel> AvailableSecurityProfiles { get; set; }

        public string GetUserFullName()
        {
            return $"{ FirstName } { LastName }";
        }
    }
}