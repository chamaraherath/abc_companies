﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models.ViewModel
{
    public class HomeViewModel
    {
        public HomeViewModel()
        {

        }
        public int CompanyCount { get; set; }
        public int EmployeeCount { get; set; }
        public int UserCount { get; set; }
    }
}