﻿using ABCCompanies.Domain;

namespace ABCCompanies.Models.ViewModel
{
    public class CompanyViewModel
    {

        public CompanyViewModel(Company company)
        {
            Id = company.Id;
            Name = company.Name;
            Email = company.Email;
            Logo = company.Logo;
            WebSite = company.Website;
        }

        public CompanyViewModel()
        {

        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Logo { get; set; }
        public string WebSite { get; set; }
    }
}