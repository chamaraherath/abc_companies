﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models.ViewModel
{
    public class LoginViewModel
    {
        public string UserEmail { get; set; }
        public string Password { get; set; }
    }
}