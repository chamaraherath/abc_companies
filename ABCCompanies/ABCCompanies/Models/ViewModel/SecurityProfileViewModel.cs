﻿using ABCCompanies.Domain;

namespace ABCCompanies.Models.ViewModel
{
    public class SecurityProfileViewModel
    {
        public SecurityProfileViewModel()
        {

        }

        public SecurityProfileViewModel(SecurityProfile securityProfile)
        {
            Id = securityProfile.Id;
            Name = securityProfile.Name;
            CanCreate = securityProfile.CanCreate;
            CanRead = securityProfile.CanRead;
            CanUpdate = securityProfile.CanUpdate;
            CanDelete = securityProfile.CanDelete;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool CanCreate { get; set; }
        public bool CanRead { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }

    }
}