﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("User");
            HasKey(s => s.Id);

            HasRequired(r => r.SecurityProfile)
            .WithMany()
            .HasForeignKey(t => t.SecurityProfileId);
        }
    }
}