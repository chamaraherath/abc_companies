﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models.Configurations
{
    public class SecurityProfileConfiguration : EntityTypeConfiguration<SecurityProfile>
    {
        public SecurityProfileConfiguration()
        {
            ToTable("SecurityProfile");
            HasKey(s => s.Id);

            HasMany(r => r.Users)
           .WithRequired(c => c.SecurityProfile)
           .HasForeignKey(t => t.SecurityProfileId);
        }
    }
}