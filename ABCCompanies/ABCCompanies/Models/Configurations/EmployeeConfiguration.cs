﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models.Configurations
{
    public class EmployeeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration()
        {
            ToTable("Employee");
            HasKey(s => s.Id);

            HasRequired(r => r.Company)
            .WithMany()
            .HasForeignKey(t => t.CompanyId);

            HasMany(r => r.Tags)
            .WithMany(r => r.Employees)
            .Map(mc =>
            {
                mc.ToTable("TagEmployee");
                mc.MapLeftKey("TagId");
                mc.MapRightKey("EmployeeId");
            });
        }
    }
}