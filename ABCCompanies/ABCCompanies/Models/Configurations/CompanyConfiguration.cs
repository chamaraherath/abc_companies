﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models.Configurations
{
    public class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            ToTable("Company");
            HasKey(s => s.Id);

            HasMany(r => r.Employees)
            .WithRequired(c => c.Company)
            .HasForeignKey(t => t.CompanyId);
        }
    }
}