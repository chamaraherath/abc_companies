﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABCCompanies.Models.Contracts
{
    public interface IUserRepository : IEntityRepository<User>
    {
        User GetUserByEmail(string email);
    }
}
