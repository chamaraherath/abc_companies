﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABCCompanies.Models.Contracts
{
    public interface IUnitOfWork
    {
        IEntityRepository<SecurityProfile> SecurityProfiles { get; }
        IUserRepository Users { get; }
        IEntityRepository<Company> Companies { get; }
        IEntityRepository<Employee> Employees { get; }
        int Save();
    }
}
