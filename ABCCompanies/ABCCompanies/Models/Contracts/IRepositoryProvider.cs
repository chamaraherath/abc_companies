﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABCCompanies.Models.Contracts
{
    public interface IRepositoryProvider
    {
        DbContext DbContext { get; set; }

        IEntityRepository<T> GetRepositoryForEntityType<T>() where T : class;

        T GetRepository<T>(Func<DbContext, object> factory = null) where T : class;

        void SetRepository<T>(T Repository);
    }
}
