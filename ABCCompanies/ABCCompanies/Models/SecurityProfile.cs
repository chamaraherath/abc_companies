﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models
{
    public class SecurityProfile
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool CanCreate { get; set; }
        public bool CanRead { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }
        public ICollection<User> Users { get; set; }
    }
}