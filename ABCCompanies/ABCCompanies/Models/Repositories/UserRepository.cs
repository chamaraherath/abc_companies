﻿using ABCCompanies.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models.Repositories
{
    public class UserRepository : EntityRepository<User>, IUserRepository
    {
        public UserRepository(DbContext dbContext) : base(dbContext)
        {

        }
        public User GetUserByEmail(string email)
        {
            return DbSet.OfType<User>().First(u => u.Email.Equals(email));
        }
    }
}