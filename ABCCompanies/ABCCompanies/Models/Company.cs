﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Logo { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}