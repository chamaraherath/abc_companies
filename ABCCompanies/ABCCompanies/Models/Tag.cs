﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models
{
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}