﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ABCCompanies.Models
{
    public class CompanyDbContext : DbContext
    {
        public CompanyDbContext() : base(System.Configuration.ConfigurationManager.ConnectionStrings["CompanyContext"].ConnectionString ?? "name=CompanyDbContext")
        {
        }

        public virtual DbSet<SecurityProfile> SecurityProfiles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
    }
}