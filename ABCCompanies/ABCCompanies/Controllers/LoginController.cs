﻿using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ABCCompanies.Data.Contracts;
using ABCCompanies.Infrastructure;
using ABCCompanies.Models.ViewModel;

namespace ABCCompanies.Controllers
{
    public class LoginController : Controller
    {
        IUnitOfWork UnitOfWork;
        public LoginController(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid || loginViewModel.UserEmail == null || loginViewModel.Password == null ||
                loginViewModel.UserEmail.Trim().Length == 0 || loginViewModel.Password.Trim().Length == 0) {
                TempData["ErrorMessages"] = "Provided creadentials are not valid.!";
                return RedirectToAction("Index");
            }

            var user = UnitOfWork.Users.GetUserByEmail(loginViewModel.UserEmail);
            if (user.Password == loginViewModel.Password) {
                Session["logged_user_id"] = user.Id;

                CustomSerializeModel userModel = new CustomSerializeModel()
                {
                    UserId = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    CanCreate = user.SecurityProfile.CanCreate,
                    CanRead = user.SecurityProfile.CanRead,
                    CanUpdate = user.SecurityProfile.CanUpdate,
                    CanDelete = user.SecurityProfile.CanDelete,
                };

                string userData = JsonConvert.SerializeObject(userModel);
                FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1, user.Email, DateTime.Now,
                    DateTime.Now.AddMinutes(15), false, userData);

                string enTicket = FormsAuthentication.Encrypt(authTicket);
                HttpCookie faCookie = new HttpCookie("Cookie1", enTicket);
                Response.Cookies.Add(faCookie);

                return RedirectToAction("Index", "Home");
            }
            TempData["ErrorMessages"] = "User name or password incorrect.!";
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Remove("logged_user_id");
            return RedirectToAction("Index", "Home");
        }
    }
}