﻿using System;
using System.Linq;
using System.Web.Mvc;
using ABCCompanies.Data.Contracts;
using ABCCompanies.Domain;
using ABCCompanies.Infrastructure;
using ABCCompanies.Models.ViewModel;

namespace ABCCompanies.Controllers
{
    [CustomAuthenticationFilter]
    public class UserController : BaseController
    {
        public UserController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }
        // GET: User
        [CustomAuthorize("CanRead")]
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize("CanRead")]
        public PartialViewResult GetUserListPartial()
        {
            var users = UnitOfWork.Users.GetAll().ToList().Select(u => new UserViewModel(u));
            return PartialView("_UserList", users);
        }

        [CustomAuthorize("CanRead")]
        public PartialViewResult GetAddUserPartial()
        {
            var securityProfiles = UnitOfWork.SecurityProfiles.GetAll().ToList().Select(s => new SecurityProfileViewModel(s)).ToList();
            var newUserViewModel = new UserViewModel
            {
                AvailableSecurityProfiles = securityProfiles
            };
            return PartialView("_AddUser", newUserViewModel);
        }

        [CustomAuthorize("CanCreate")]
        public ActionResult Add(UserViewModel userViewModel)
        {
            if (userViewModel == null) {
                throw new InvalidOperationException();
            }

            var duplicateUser = UnitOfWork.Users.GetAll().FirstOrDefault(u => u.Email == userViewModel.Email.Trim());

            if (duplicateUser != null) {
                TempData["ErrorMessages"] = "Provided email already in used";
                return RedirectToAction("Index");
            }

            if (userViewModel.Password != userViewModel.ValidatePassword) {
                TempData["ErrorMessages"] = "Password validation faild - Re-type Password is not matching.";
                return RedirectToAction("Index");
            }

            var newUser = new User
            {
                FirstName = userViewModel.FirstName,
                LastName = userViewModel.LastName,
                Email = userViewModel.Email,
                Password = userViewModel.Password,
                SecurityProfile = UnitOfWork.SecurityProfiles.GetById(userViewModel.SecurityProfile.Id)
            };

            UnitOfWork.Users.Add(newUser);
            TempData["Messages"] = UnitOfWork.Save() > 0 ? "User created successfully" : "User creation faild. Please try again";
            return RedirectToAction("Index");
        }

        [CustomAuthorize("CanUpdate")]
        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var user = new UserViewModel(UnitOfWork.Users.GetById(id));

            if (user == null) {
                throw new Exception("Unable to find selected user.");
            }

            user.AvailableSecurityProfiles = UnitOfWork.SecurityProfiles.GetAll()
                                                                        .ToList()
                                                                        .Select(e => new SecurityProfileViewModel(e))
                                                                        .ToList();

            return PartialView("_EditUser", user);
        }

        [CustomAuthorize("CanUpdate")]
        [HttpPost]
        public ActionResult Edit(UserViewModel userViewModel)
        {
            if (userViewModel == null) {
                throw new Exception("Unable to edit user with given details.");
            }

            var user = UnitOfWork.Users.GetById(userViewModel.Id.GetValueOrDefault());

            if (user == null) {
                throw new Exception("User not available in the system.");
            }

            user.FirstName = userViewModel.FirstName;
            user.LastName = userViewModel.LastName;
            user.SecurityProfileId = userViewModel.SecurityProfile.Id;

            if (User.Identity.Name == user.Email) {
                user.Email = userViewModel.Email;

                if (userViewModel.Password != null && userViewModel.Password.Trim() != "" && userViewModel.Password != user.Password) {
                    if (userViewModel.OldPassword != user.Password) {
                        TempData["ErrorMessages"] = "Old Password is not valid";
                        return RedirectToAction("Index");
                    }
                    if (userViewModel.Password != userViewModel.ValidatePassword) {
                        TempData["ErrorMessages"] = "Re-type password is not matching.";
                        return RedirectToAction("Index");
                    }
                    user.Password = userViewModel.Password;
                }            
            }

            UnitOfWork.Users.Update(user);
            if (UnitOfWork.Save() > 0)
                TempData["Messages"] = "User update successfully.";
            else
                TempData["ErrorMessages"] = "User update faild. Please try again.";
            return RedirectToAction("Index");
        }

        [CustomAuthorize("CanDelete")]
        public ActionResult Delete(int id)
        {
            var user = UnitOfWork.Users.GetById(id);

            if (user == null) {
                throw new Exception("Unable to find user for delete.");
            }

            UnitOfWork.Users.Delete(user);
            if (UnitOfWork.Save() > 0)
                TempData["Messages"] = "User deleted successfully.";
            else
                TempData["ErrorMessages"] = "User delete faild. Please try again.";
            return RedirectToAction("Index");
        }
    }
}