﻿using System;
using System.Linq;
using System.Web.Mvc;
using ABCCompanies.Data.Contracts;
using ABCCompanies.Domain;
using ABCCompanies.Infrastructure;
using ABCCompanies.Models.ViewModel;

namespace ABCCompanies.Controllers
{
    [CustomAuthenticationFilter]
    public class EmployeeController : BaseController
    {
        public EmployeeController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: Employee
        [CustomAuthorize("CanRead")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [CustomAuthorize("CanRead")]
        public PartialViewResult GetEmployeesByCompanyId(int companyId)
        {
            var employees = UnitOfWork.Employees.GetAll()
                                                .Where(e => e.Company.Id == companyId)
                                                .ToList()
                                                .Select(e => new EmployeeViewModel(e));
            return PartialView("_EmployeeList", employees);
        }

        [HttpGet]
        [CustomAuthorize("CanRead")]
        public PartialViewResult GetEmployeesListPartial()
        {
            var employees = UnitOfWork.Employees.GetAll().ToList().Select(e => new EmployeeViewModel(e));
            return PartialView("_EmployeeList", employees);
        }


        [HttpGet]
        [CustomAuthorize("CanRead")]
        public PartialViewResult GetAddEmployeePartial(int companyId)
        {
            var companies = UnitOfWork.Companies.GetAll().ToList().Select(e => new CompanyViewModel(e)).ToList();
            var newEmployeeViewModel = new EmployeeViewModel
            {
                Company = companyId != 0 ? new CompanyViewModel(UnitOfWork.Companies.GetById(companyId)) : new CompanyViewModel(),
                AvailableCompanies = companies
            };
            return PartialView("_AddEmployee", newEmployeeViewModel);
        }

        [HttpPost]
        [CustomAuthorize("CanCreate")]
        public ActionResult Add(EmployeeViewModel employeeViewModel)
        {
            if (employeeViewModel == null) {
                throw new InvalidOperationException();
            }
            var newEmployee = new Employee
            {
                FirstName = employeeViewModel.FirstName,
                LastName = employeeViewModel.LastName,
                Email = employeeViewModel.Email,
                Phone = employeeViewModel.Phone,
                Company = UnitOfWork.Companies.GetById(employeeViewModel.Company.Id),
            };

            UnitOfWork.Employees.Add(newEmployee);
            TempData["Messages"] = UnitOfWork.Save() > 0 ? "Employee created successfully" : "Employee creation faild. Please try again";
            return RedirectToAction("Index");
        }

        [CustomAuthorize("CanUpdate")]
        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var employee = new EmployeeViewModel(UnitOfWork.Employees.GetById(id));

            if (employee == null) {
                throw new Exception("Unable to find selected employee.");
            }

            employee.AvailableCompanies = UnitOfWork.Companies.GetAll().ToList().Select(e => new CompanyViewModel(e)).ToList();

            return PartialView("_EditEmployee", employee);
        }

        [CustomAuthorize("CanUpdate")]
        [HttpPost]
        public ActionResult Edit(EmployeeViewModel employeeViewModel)
        {
            if (employeeViewModel == null) {
                throw new Exception("Unable to edit employee with given details.");
            }

            var employee = UnitOfWork.Employees.GetById(employeeViewModel.Id);

            employee.FirstName = employeeViewModel.FirstName;
            employee.LastName = employeeViewModel.LastName;
            employee.Email = employeeViewModel.Email;
            employee.Phone = employeeViewModel.Phone;
            employee.CompanyId = employeeViewModel.Company.Id;

            UnitOfWork.Employees.Update(employee);
            if (UnitOfWork.Save() > 0)
                TempData["Messages"] = "Employee update successfully.";
            else
                TempData["ErrorMessages"] = "Employee update faild. Please try again.";
            return RedirectToAction("Index");
        }

        [CustomAuthorize("CanDelete")]
        public ActionResult Delete(int id)
        {
            var employee = UnitOfWork.Employees.GetById(id);

            if (employee == null) {
                throw new Exception("Unable to find employee for delete.");
            }

            UnitOfWork.Employees.Delete(employee);
            if (UnitOfWork.Save() > 0)
                TempData["Messages"] = "Employee deleted successfully.";
            else
                TempData["ErrorMessages"] = "Employee delete faild. Please try again.";
            return RedirectToAction("Index");
        }
    }
}