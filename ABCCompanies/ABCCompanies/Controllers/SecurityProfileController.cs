﻿using ABCCompanies.Data.Contracts;
using ABCCompanies.Domain;
using ABCCompanies.Infrastructure;
using ABCCompanies.Models.ViewModel;
using System;
using System.Linq;
using System.Web.Mvc;

namespace ABCCompanies.Controllers
{
    [CustomAuthenticationFilter]
    public class SecurityProfileController : BaseController
    {
        public SecurityProfileController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: SecurityProfile
        [CustomAuthorize("CanRead")]
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize("CanRead")]
        public PartialViewResult GetSecurityProfileListPartial()
        {
            var securityProfiles = UnitOfWork.SecurityProfiles.GetAll().ToList().Select(s => new SecurityProfileViewModel(s));
            return PartialView("_SecurityProfileList", securityProfiles);
        }

        [HttpPost]
        [CustomAuthorize("CanCreate")]
        public ActionResult Add(SecurityProfileViewModel securityProfileViewModel)
        {
            if (securityProfileViewModel == null) {
                throw new InvalidOperationException();
            }

            var newSecurityProfile = new SecurityProfile
            {
                Name = securityProfileViewModel.Name,
                CanCreate = securityProfileViewModel.CanCreate,
                CanRead = securityProfileViewModel.CanRead,
                CanUpdate = securityProfileViewModel.CanUpdate,
                CanDelete = securityProfileViewModel.CanDelete
            };

            UnitOfWork.SecurityProfiles.Add(newSecurityProfile);
            TempData["Messages"] = UnitOfWork.Save() > 0 ? "Security profile created successfully" : "Security profile creation faild. Please try again";
            return RedirectToAction("Index");
        }


        [CustomAuthorize("CanUpdate")]
        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var securityProfile = new SecurityProfileViewModel(UnitOfWork.SecurityProfiles.GetById(id));

            if (securityProfile == null) {
                throw new Exception("Unable to find selected profile.");
            }

            return PartialView("_EditSecurityProfile", securityProfile);
        }

        [CustomAuthorize("CanUpdate")]
        [HttpPost]
        public ActionResult Edit(SecurityProfileViewModel securityProfileViewModel)
        {
            if (securityProfileViewModel == null) {
                throw new Exception("Unable to edit security profile with given details.");
            }

            var profile = UnitOfWork.SecurityProfiles.GetById(securityProfileViewModel.Id);

            profile.Name = securityProfileViewModel.Name;
            profile.CanCreate = securityProfileViewModel.CanCreate;
            profile.CanRead = securityProfileViewModel.CanRead;
            profile.CanUpdate = securityProfileViewModel.CanUpdate;
            profile.CanDelete = securityProfileViewModel.CanDelete;

            UnitOfWork.SecurityProfiles.Update(profile);
            if (UnitOfWork.Save() > 0)
                TempData["Messages"] = "Security profile update successfully.";
            else
                TempData["ErrorMessages"] = "Security profile update faild. Please try again.";
            return RedirectToAction("Index");
        }

        [CustomAuthorize("CanDelete")]
        public ActionResult Delete(int id)
        {
            var profile = UnitOfWork.SecurityProfiles.GetById(id);

            if (profile == null) {
                throw new Exception("Unable to find security profile for delete.");
            }

            if (profile.Users.Count > 0) {
                TempData["ErrorMessages"] = "Unable to delete security profile. There are number of user(s) assocites with it.";
                return RedirectToAction("Index");
            }

            UnitOfWork.SecurityProfiles.Delete(profile);
            if (UnitOfWork.Save() > 0)
                TempData["Messages"] = "security profile deleted successfully.";
            else
                TempData["ErrorMessages"] = "security profile delete faild. Please try again.";
            return RedirectToAction("Index");
        }
    }
}