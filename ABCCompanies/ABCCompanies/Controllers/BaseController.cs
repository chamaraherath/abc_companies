﻿using System.Web.Mvc;
using System.Web.Routing;
using ABCCompanies.Data.Contracts;
using ABCCompanies.Models.ViewModel;

namespace ABCCompanies.Controllers
{
    public class BaseController : Controller
    {
        protected IUnitOfWork UnitOfWork;
        public UserViewModel UserViewModel;
        public BaseController(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
            UserViewModel = new UserViewModel(null);
        }

        protected sealed override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            int sessionUserId = 0;
            if (Session["logged_user_id"] != null && int.TryParse(Session["logged_user_id"].ToString(), out sessionUserId))
            {
                var loggedUser = UnitOfWork.Users.GetById(sessionUserId);
                if (loggedUser != null)
                {
                    UserViewModel = new UserViewModel(loggedUser);
                    Session["logged_user_id"] = loggedUser.Id;
                }
                else
                {
                    Logout(filterContext);
                }
            }
            else
            {
                Logout(filterContext);
            }
        }

        private void Logout(ActionExecutingContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary{
                    {"controller", "Login"},
                    {"action", "Index"}
                });
        }
    }
}