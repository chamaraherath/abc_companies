﻿using ABCCompanies.Data.Contracts;
using ABCCompanies.Infrastructure;
using ABCCompanies.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABCCompanies.Controllers
{
    [CustomAuthenticationFilter]
    public class HomeController : BaseController
    {
        public HomeController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {

        }

        [CustomAuthorize("CanRead")]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            var homeViewModel = new HomeViewModel
            {
                CompanyCount = UnitOfWork.Companies.GetAll().Count(),
                EmployeeCount = UnitOfWork.Employees.GetAll().Count(),
                UserCount = UnitOfWork.Users.GetAll().Count(),
            };
            return View(homeViewModel);
        }

        public PartialViewResult RendererTopBar()
        {
            return PartialView("_LayoutTopBar", UserViewModel);
        }

        public ActionResult UnAuthorized() {
          return View();
        }
    }
}
