﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ABCCompanies.Data.Contracts;
using ABCCompanies.Domain;
using ABCCompanies.Infrastructure;
using ABCCompanies.Models.ViewModel;

namespace ABCCompanies.Controllers
{
    [CustomAuthenticationFilter]
    public class CompanyController : BaseController
    {
        public CompanyController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        // GET: Company
        [CustomAuthorize("CanRead")]
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize("CanRead")]
        public PartialViewResult GetCompaniesListPartial()
        {
            var companies = UnitOfWork.Companies.GetAll().ToList().Select(c => new CompanyViewModel(c));
            return PartialView("_CompanyList", companies);
        }

        [HttpPost]
        [CustomAuthorize("CanCreate")]
        public ActionResult Add(CompanyViewModel companyViewModel)
        {
            if (companyViewModel == null) {
                throw new InvalidOperationException();
            }

            var newCompany = new Company
            {
                Name = companyViewModel.Name,
                Email = companyViewModel.Email,
                Website = companyViewModel.WebSite
            };
            HttpPostedFileBase postedFile = Request.Files["LogoFile"];
            if (postedFile.FileName != "") {
                newCompany.Logo = UploadFile(postedFile);
            }


            UnitOfWork.Companies.Add(newCompany);
            TempData["Messages"] = UnitOfWork.Save() > 0 ? "Company created successfully." : "Company creation faild. Please try again.";
            return RedirectToAction("Index");
        }

        private string UploadFile(HttpPostedFileBase uploadedFile)
        {
            if (uploadedFile != null) {
                string extension = Path.GetExtension(uploadedFile.FileName);
                if (extension.ToLower() == ".png" || extension.ToLower() == ".jpg") {
                    Stream strm = uploadedFile.InputStream;
                    using (var image = System.Drawing.Image.FromStream(strm)) {
                        int newWidth = 200;
                        int newHeight = 200;
                        var thumbImg = new Bitmap(newWidth, newHeight);
                        var thumbGraph = Graphics.FromImage(thumbImg);
                        thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                        thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                        thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        var imgRectangle = new Rectangle(0, 0, newWidth, newHeight);
                        thumbGraph.DrawImage(image, imgRectangle);
                        string relativePath = @"Content\Images\CompanyImages\";
                        string newFileName = Guid.NewGuid() + extension;
                        string targetPath = Server.MapPath(@"~\" + relativePath) + newFileName;
                        thumbImg.Save(targetPath, image.RawFormat);
                        return relativePath + newFileName;
                    }
                }
            }
            return string.Empty;
        }

        [CustomAuthorize("CanUpdate")]
        [HttpGet]
        public PartialViewResult Edit(int id)
        {
            var company = UnitOfWork.Companies.GetById(id);

            if (company == null) {
                throw new Exception("Unable to find selected company.");
            }

            return PartialView("_EditCompany", new CompanyViewModel(company));
        }

        [CustomAuthorize("CanUpdate")]
        [HttpPost]
        public ActionResult Edit(CompanyViewModel companyViewModel)
        {
            if (companyViewModel == null) {
                throw new Exception("Unable to edit company with given details.");
            }

            var company = UnitOfWork.Companies.GetById(companyViewModel.Id);

            company.Name = companyViewModel.Name;
            company.Email = companyViewModel.Email;
            company.Website = companyViewModel.WebSite;


            HttpPostedFileBase postedFile = Request.Files["LogoFile"];

            if (postedFile.FileName != "") {
                company.Logo = UploadFile(postedFile);
            }

            UnitOfWork.Companies.Update(company);
            if (UnitOfWork.Save() > 0)
                TempData["Messages"] = "Company update successfully.";
            else
                TempData["ErrorMessages"] = "Company update faild. Please try again.";
            return RedirectToAction("Index");
        }

        [CustomAuthorize("CanDelete")]
        public ActionResult Delete(int id)
        {
            var company = UnitOfWork.Companies.GetById(id);

            if (company == null) {
                throw new Exception("Unable to find company for delete.");
            }

            if (company.Employees.Count > 0) {
                TempData["ErrorMessages"] = "Unable to delete company. There are number of employee(s) assocites with it.";
                return RedirectToAction("Index");
            }

            UnitOfWork.Companies.Delete(company);
            if (UnitOfWork.Save() > 0)
                TempData["Messages"] = "Company deleted successfully.";
            else
                TempData["ErrorMessages"] = "Company delete faild. Please try again.";
            return RedirectToAction("Index");
        }

    }
}