﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace ABCCompanies.Infrastructure
{
    public class CustomPrincipal : IPrincipal
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool CanCreate { get; set; }
        public bool CanRead { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }

        public CustomPrincipal(string username)
        {
            Identity = new GenericIdentity(username);
        }
        public IIdentity Identity
        {
            get; private set;
        }

        public bool IsInRole(string role)
        {
            switch (role)
            {
                case "CanCreate":
                    return CanCreate;
                case "CanRead":
                    return CanRead;
                case "CanUpdate":
                    return CanUpdate;
                case "CanDelete":
                    return CanDelete;
                default:
                    return false;
            }
        }
    }
}