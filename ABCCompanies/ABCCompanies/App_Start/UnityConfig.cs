using System.Web.Mvc;
using Unity;
using Unity.Lifetime;
using Unity.Mvc5;
using ABCCompanies.Data.Contracts;
using ABCCompanies.Data.Repositories;
using ABCCompanies.Models;

namespace ABCCompanies
{
    public static class UnityConfig
    {
        public static UnityContainer container { get; set; }
        public static void RegisterComponents()
        {
			container = new UnityContainer();
            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container
                .RegisterType<RepositoryFactories>(new ContainerControlledLifetimeManager())
                .RegisterType<IUnitOfWork, UnitOfWork>(new PerResolveLifetimeManager())
                .RegisterType<IRepositoryProvider, RepositoryProvider>(new PerResolveLifetimeManager())
                .RegisterType<IUserRepository, UserRepository>(new PerResolveLifetimeManager());


            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}